﻿using System;
using TourOffice.Dto.Mod;

namespace TourOffice.Service
{
  public interface IHotelService : IDomainEntityService<HotelDto>
  {
    Guid Create(string name, string descriotion, string imageUrl, int category);

    void AddService( Guid hotelId, Guid serviceId);
  }
}
