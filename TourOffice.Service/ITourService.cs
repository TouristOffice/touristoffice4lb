﻿using System;
using TourOffice.Dto.Mod;

namespace TourOffice.Service
{
  public interface ITourService : IDomainEntityService<TourDto>
  {
    Guid Create(string name, Guid hotel);

    void AddTourItem(Guid tourId, Guid tourItemId);
  }
}
