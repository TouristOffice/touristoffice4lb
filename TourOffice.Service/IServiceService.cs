﻿using System;
using TourOffice.Dto.Mod;

namespace TourOffice.Service
{
  public interface IServiceService : IDomainEntityService<ServiceDto>
  {
    Guid Create(string name);
  }
}
