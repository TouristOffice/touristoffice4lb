﻿using System;
using System.Collections.Generic;
using TourOffice.Dto;

namespace TourOffice.Service
{
  public interface IDomainEntityService<out TDto> where TDto : DomainEntityDto<TDto>
  {
    IList<Guid> ViewAll ();

    TDto View (Guid domainId);
  }
}
