﻿using System;
using TourOffice.Dto.Mod;

namespace TourOffice.Service
{
  public interface ICountryService : IDomainEntityService<CountryDto>
  {
    Guid Create(string name, string about);

    void AddRegion(Guid countryId, Guid regionId);
  }
}
