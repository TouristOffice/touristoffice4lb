﻿using System;
using System.Collections.Generic;
using TourOffice.Dto.Mod;

namespace TourOffice.Service
{
  public interface IOrderService : IDomainEntityService<OrderDto>
  {
    IList<Guid> ViewUnconfirmed ();

    IList<Guid> ViewReady4Delivery ();

    Guid CreateNew (Guid assignmentTourId);

    void SetDiscount (Guid orderId, decimal discountPercent);

    void Confirm (Guid orderId);
    
    void Cancel (Guid orderId);
  }
}
