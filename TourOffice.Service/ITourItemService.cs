﻿using System;
using TourOffice.Dto.Mod;

namespace TourOffice.Service
{
  public interface ITourItemService : IDomainEntityService<TourItemDto>
  {
    Guid Create(int quantityNight, DateTime departureDate, decimal price);
  }
}
