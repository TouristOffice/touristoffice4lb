﻿using System;
using TourOffice.Dto.Mod;

namespace TourOffice.Service
{
  public interface IAssignmentTourService : IDomainEntityService<AssignmentTourDto>
  {
    Guid Create(Guid tourItemId, Guid hotelId);
  }
}
