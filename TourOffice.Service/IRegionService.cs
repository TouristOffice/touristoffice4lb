﻿using System;
using TourOffice.Dto.Mod;

namespace TourOffice.Service
{
  public interface IRegionService : IDomainEntityService<RegionDto>
  {
    Guid Create(string name);

    void AddTour(Guid regionId, Guid tourId);
  }
}
