﻿using System;
using System.Linq;
using TourOffice.Model.Mod;

namespace TourOffice.Repository.Mod
{
  public interface IOrderRepository : IRepository<Order>
  {
    IQueryable<Guid> SelectUnconfirmedIds ();

    IQueryable<Guid> SelectReady4DeliveryIds ();
  }
}
