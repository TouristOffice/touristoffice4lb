﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.Mod
{
    public interface IHotelRepository : IRepository<Hotel>
    {
    }
}
