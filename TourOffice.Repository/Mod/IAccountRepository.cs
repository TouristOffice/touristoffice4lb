﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.Mod
{
  public interface IAccountRepository : IRepository<Account>
  {
    Account FindByOrder (Order o);

    Account FindByEmail (string email);
  }
}
