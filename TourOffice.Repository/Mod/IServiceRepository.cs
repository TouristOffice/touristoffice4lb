﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.Mod
{
  public interface IServiceRepository : IRepository<Service>
  {
  }
}
