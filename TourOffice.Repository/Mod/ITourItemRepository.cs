﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.Mod
{
  public interface ITourItemRepository : IRepository<TourItem>
  {
  }
}
