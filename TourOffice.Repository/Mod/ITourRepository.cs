﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.Mod
{
  public interface ITourRepository : IRepository<Tour>
  {
  }
}
