﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.Mod
{
    public interface IAssignmentTourRepository : IRepository<AssignmentTour>
    {
    }
}
