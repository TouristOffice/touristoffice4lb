﻿using System;
using System.Linq;
using TourOffice.Utils;

namespace TourOffice.Repository
{
  public interface IRepository<T> where T : Entity
  {
    void StartTransaction ();

    void Commit ();

    void Rollback ();

    int Count ();

    T Load (int id);

    IQueryable<T> LoadAll ();

    void Add (T t);

    void Delete (T t);

    T FindByDomainId (Guid domainId);

    IQueryable<Guid> SelectAllDomainIds ();
  }
}
