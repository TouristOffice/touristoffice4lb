﻿using System;
using System.Collections.Generic;

namespace TourOffice.Dto.Mod
{
  public class TourItemDto: DomainEntityDto<TourItemDto>
  {
    public DateTime DepartureDate { get; }
    public int QuantityNight { get; }
    public decimal Price { get; }

    public TourItemDto(Guid domainId, DateTime departureDate, int quantityNight, decimal price) 
      : base(domainId)
    {
      DepartureDate = departureDate;
      QuantityNight = quantityNight;
      Price = price;
    }

    protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
    {
      return new List<object> {DepartureDate, QuantityNight, Price};
    }

    public override string ToString ()
    {
      return string.Format("Id = {0}\nDepartureDate = {1}\nQuantityNight = {2}\nPrice = {3}\n", 
        DomainId, DepartureDate, QuantityNight, Price);
    }
  }
}
