﻿using System;
using System.Collections.Generic;

namespace TourOffice.Dto.Mod
{
  public class RegionDto: DomainEntityDto<RegionDto>
  {
    public string Name { get; }
    public virtual ICollection<TourDto> Tours { get; }

    public RegionDto (Guid domainId, string name, ICollection<TourDto> tours) : base(domainId)
    {
      Name = name;
      Tours = tours;
    }

    protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
    {
      return new List<object> {Name, Tours};
    }

    public override string ToString ()
    {
      return string.Format("Id = {0}\nName = {1}\nTours:\n{2}\n", 
        DomainId, Name, string.Join("\n", Tours));
    }
  }
}
