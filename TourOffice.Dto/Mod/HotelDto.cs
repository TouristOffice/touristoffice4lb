﻿using System;
using System.Collections.Generic;

namespace TourOffice.Dto.Mod
{
  public class HotelDto: DomainEntityDto<HotelDto>
  {
    public string Name { get; }
    public int Category { get; }
    public string Description { get; }
    public virtual ICollection<ServiceDto> Services { get; }

    public HotelDto (Guid domainId, string name, int category, string description, ICollection<ServiceDto> services) : base(domainId)
    {
      Name = name;
      Category = category;
      Description = description;
      Services = services;
    }

    protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
    {
      return new List<object> {Name, Category, Description, Services};
    }

    public override string ToString ()
    {
      return string.Format("Id = {0}\nName = {1}\nCategory = {2}\nDescription = {3}\nServices:\n{4}\n", 
        DomainId,
        Name,
        Category,
        Description,
        string.Join("\n", Services));
    }
  }
}
