﻿using System;
using System.Collections.Generic;

namespace TourOffice.Dto.Mod
{
  public class TourDto: DomainEntityDto<TourDto>
  {
    public Guid HotelId { get; }
    public ICollection<TourItemDto> TourItems { get; }

    public TourDto (Guid domainId, Guid hotelId, ICollection<TourItemDto> tourItems) : base(domainId)
    {
      HotelId = hotelId;
      TourItems = tourItems;
    }

    protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
    {
      return new List<object> {HotelId, TourItems};
    }

    public override string ToString ()
    {
      return string.Format("Id = {0}\nHotelId = {1}\nTour items:\n{2}", 
        DomainId,
        HotelId,
        string.Join("\n", TourItems));
    }
  }
}
