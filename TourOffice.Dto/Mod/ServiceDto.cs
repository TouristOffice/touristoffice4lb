﻿using System;
using System.Collections.Generic;

namespace TourOffice.Dto.Mod
{
  public class ServiceDto: DomainEntityDto<ServiceDto>
  {
    public string Name { get; }

    public ServiceDto(Guid domainId, string name) : base(domainId)
    {
      Name = name;
    }

    protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
    {
      return new List<object> {Name};
    }

    public override string ToString ()
    {
      return string.Format("Id = {0}\nName = {1}\n", DomainId, Name);
    }
  }
}
