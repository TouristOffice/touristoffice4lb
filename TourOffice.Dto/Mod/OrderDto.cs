﻿using System;
using System.Collections.Generic;

namespace TourOffice.Dto.Mod
{
  public class OrderDto : DomainEntityDto<OrderDto>
  {
    public decimal DiscountPercentage { get; }

    public string Status { get; }

    public DateTime PlacementTime { get; }

    public decimal BasicCost { get; }

    public decimal TotalCost { get; }

    public Guid AssignmentTour { get; }

    public OrderDto(
      Guid domainId, 
      decimal discountPercentage, 
      string status, 
      DateTime placementTime, 
      decimal baseCost, 
      decimal totalCost,
      Guid assignmentTour
      ) : base(domainId)
    {
      DiscountPercentage = discountPercentage;
      Status = status;
      PlacementTime = placementTime;
      BasicCost = baseCost;
      TotalCost = totalCost;
      AssignmentTour = assignmentTour;
    }

    protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
    {
      return new List<object> { BasicCost };
    }

    public override string ToString ()
    {
      return string.Format("OrderId = {0}\nStatus = {1}\nPlaced = {2}\nBasic Cost = {3}\nTotal Cost = {4}\nDiscount = {5}\nAssignment tour = {6}",
                  DomainId,
                  Status,
                  PlacementTime,
                  BasicCost,
                  TotalCost,
                  DiscountPercentage,
                  AssignmentTour);
    }
  }
}
