﻿using System;
using System.Collections.Generic;

namespace TourOffice.Dto.Mod
{
  public class CountryDto: DomainEntityDto<CountryDto>
  {
    public string Name { get; }
    public string About { get; }
    public ICollection<RegionDto> Regions { get; }

    public CountryDto(Guid domainId, string name, string about, ICollection<RegionDto> regions) : base(domainId)
    {
      Name = name;
      About = about;
      Regions = regions;
    }

    protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
    {
      return new List < object >{ DomainId, Name, About, Regions};
    }

    public override string ToString ()
    {
      return string.Format("Id = {0}\nName = {1}\nAbout country = {2}\nRegions:{3}\n", 
        DomainId, Name, About, string.Join("\n", Regions));
    }
  }
}
