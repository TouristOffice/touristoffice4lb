﻿using System;
using System.Collections.Generic;

namespace TourOffice.Dto.Mod
{
  public class AccountDto : DomainEntityDto<AccountDto>
  {
    public string Name { get; private set; }
    public string Email { get; private set; }

    public AccountDto (Guid domainId, string name, string email)
        : base(domainId)
    {
      Name = name;
      Email = email;
    }

    protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck ()
    {
      return new List<object> { DomainId, Name, Email };
    }

    public override string ToString ()
    {
      return string.Format("AccountId = {0}\nName = {1}\nEmail = {2}\n", DomainId, Name, Email);
    }
  }
}
