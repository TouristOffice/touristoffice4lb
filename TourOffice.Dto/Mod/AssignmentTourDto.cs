﻿using System;
using System.Collections.Generic;

namespace TourOffice.Dto.Mod
{
  public class AssignmentTourDto : DomainEntityDto<AssignmentTourDto>
  {
    public virtual Guid HotelId { get; }
    public virtual Guid TourItemId { get; }

    public AssignmentTourDto(Guid domainId, Guid hotelId, Guid tourItemId)
      : base(domainId)
    {
      HotelId = hotelId;
      TourItemId = tourItemId;
    }

    protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
    {
      return new List<object> {DomainId, HotelId, TourItemId};
    }

    public override string ToString()
    {
      return string.Format("Id = {0}\nHotelId = {1}\nTourItemId = {2}\n", 
        DomainId, HotelId, TourItemId);
    }
  }
}
