﻿using System;
using TourOffice.Utils;

namespace TourOffice.Dto
{
  public abstract class DomainEntityDto<TConcreteDto> : Value<DomainEntityDto<TConcreteDto>>
  {
    public Guid DomainId { get; private set; }

    protected DomainEntityDto (Guid domainId)
    {
      DomainId = domainId;
    }
  }
}
