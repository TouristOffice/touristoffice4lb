﻿using System.Linq;
using TourOffice.Dto.Mod;
using TourOffice.Model.Mod;

namespace TourOffice.Service.Impl
{
  static class DtoBuilder
  {
    public static AccountDto ToDto (this Account account)
    {
      return new AccountDto(account.DomainId, account.Name, account.Email);
    }

    public static OrderDto ToDto (this Order order)
    {
      return new OrderDto(
          order.DomainId,
          order.AssignedDiscount.Percent,
          order.Status.ToString(),
          order.PlacementTime,
          order.BasicCost,
          order.TotalCost,
          order.AssignedTour.DomainId
      );
    }

    public static AssignmentTourDto ToDto(this AssignmentTour assignmentTour)
    {
      return new AssignmentTourDto(
        assignmentTour.DomainId, 
        assignmentTour.AssignedHotel.DomainId, 
        assignmentTour.AssignedItemTour.DomainId);
    }

    public static TourDto ToDto (this Tour tour)
    {
      var tourItems = tour.TourItems.Select(tourItem => tourItem.ToDto()).ToList();

      return new TourDto(tour.DomainId, tour.Hotel.DomainId, tourItems);
    }

    public static TourItemDto ToDto (this TourItem tourItem)
    {
      return new TourItemDto(
        tourItem.DomainId, 
        tourItem.DepartureDate,
        tourItem.QuantityNight,
        tourItem.Price);
    }

    public static CountryDto ToDto (this Country country)
    {
      var regions = country.ListRegion.Select(i => i.ToDto()).ToList();

      return new CountryDto(country.DomainId, country.Name, country.About, regions);
    }

    public static RegionDto ToDto (this Region region)
    {
      var tours = region.ListTour.Select(tour => tour.ToDto()).ToList();

      return new RegionDto(region.DomainId, region.Name, tours);
    }

    public static HotelDto ToDto (this Hotel hotel)
    {
      var hotels = hotel.ListService.Select(servs => servs.ToDto()).ToList();

      return new HotelDto(hotel.DomainId, hotel.Name, hotel.Category, hotel.Description, hotels);
    }

    public static ServiceDto ToDto(this Model.Mod.Service service)
    {
      return new ServiceDto(service.DomainId, service.Name);
    }
  }
}
