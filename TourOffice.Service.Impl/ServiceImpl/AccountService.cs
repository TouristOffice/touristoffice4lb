﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Dto.Mod;
using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Service.Impl.ServiceImpl
{
  public class AccountService : IAccountService
  {
    private class OrderTrackingVisitor : IAccountVisitor
    {
      public OrderTrackingVisitor (Order o)
      {
        this.o = o;
      }

      public void Visit (Account account)
      {
        throw new ArgumentException("Not an operator account");
      }

      public void Visit (OperatorAccount account)
      {
        account.TrackOrder(o);
      }

      public void Visit (CustomerAccount account)
      {
        //no order
      }

      private Order o;
    }

    private class OrdersExtractVisitor : IAccountVisitor
    {
      public OrdersExtractVisitor (IList<Guid> orderIds)
      {
        this.orderIds = orderIds;
      }

      public void Visit (Account account)
      {
        // No orders..
      }

      public void Visit (OperatorAccount account)
      {
        foreach (var o in account.Orders)
          orderIds.Add(o.DomainId);
      }

      public void Visit (CustomerAccount account)
      {
        //no orders...
      }

      private IList<Guid> orderIds;
    };

    private readonly IAccountRepository _accountRepository;
    private readonly IOrderRepository _orderRepository;
    
    public AccountService (
        IAccountRepository accountRepository,
        IOrderRepository orderRepository
    )
    {
      _accountRepository = accountRepository;
      _orderRepository = orderRepository;
    }

    public IList<Guid> ViewAll ()
    {
      return _accountRepository.SelectAllDomainIds().ToList();
    }

    public AccountDto View (Guid accountId)
    {
      var a = ServiceUtils.ResolveAccount(accountId, _accountRepository);
      return a.ToDto();
    }

    public AccountDto Identify (string email, string password)
    {
      var a = _accountRepository.FindByEmail(email);
      if (a == null)
        return null;

      if (!a.CheckPassword(password))
        return null;

      return a.ToDto();
    }

    public IList<Guid> ViewAssociatedOrders (Guid accountId)
    {
      var a = ServiceUtils.ResolveAccount(accountId, _accountRepository);

      var orderIds = new List<Guid> { };
      a.Accept(new OrdersExtractVisitor(orderIds));

      return orderIds;
    }

    public Guid CreateOperator (string name, string email, string password)
    {
      var a = _accountRepository.FindByEmail(email);
      if (a != null)
        throw new ArgumentException("Duplicate account");

      _accountRepository.StartTransaction();

      var operatorAccount = new OperatorAccount(Guid.NewGuid(), name, email, password);
      _accountRepository.Add(operatorAccount);

      _accountRepository.Commit();

      return operatorAccount.DomainId;
    }

    public void ChangeName (Guid accountId, string newName)
    {
      _accountRepository.StartTransaction();

      var a = ServiceUtils.ResolveAccount(accountId, _accountRepository);
      a.Name = newName;

      _accountRepository.Commit();
    }

    public void ChangeEmail (Guid accountId, string newEmail)
    {
      _accountRepository.StartTransaction();

      var a = _accountRepository.FindByEmail(newEmail);
      if (a != null)
        throw new ArgumentException("Duplicate account");

      a.Email = newEmail;

      _accountRepository.Commit();
    }

    public void ChangePassword (Guid accountId, string oldPassword, string newPassword)
    {
      _accountRepository.StartTransaction();

      var a = ServiceUtils.ResolveAccount(accountId, _accountRepository);

      if (!a.CheckPassword(oldPassword))
        throw new ArgumentException("Password failed");

      a.Password = newPassword;

      _accountRepository.Commit();
    }

    public void TrackOrder (Guid accountId, Guid orderId)
    {
      _accountRepository.StartTransaction();

      var a = ServiceUtils.ResolveAccount(accountId, _accountRepository);
      var o = ServiceUtils.ResolveOrder(orderId, _orderRepository);

      var otherAccount = _accountRepository.FindByOrder(o);
      if (otherAccount != null)
        throw new ArgumentException("Order was already served by different account");

      a.Accept(new OrderTrackingVisitor(o));
      _accountRepository.Commit();
    }
  }
}
