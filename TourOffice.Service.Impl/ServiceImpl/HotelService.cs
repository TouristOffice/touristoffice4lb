﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Dto.Mod;
using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Service.Impl.ServiceImpl
{
  public class HotelService : IHotelService
  {
    private readonly IHotelRepository _hotelRepository;
    private readonly IServiceRepository _serviceRepository;

    public HotelService(
      IHotelRepository hotelRepository, 
      IServiceRepository serviceRepository)
    {
      _hotelRepository = hotelRepository;
      _serviceRepository = serviceRepository;
    }

    public IList<Guid> ViewAll ()
    {
      return _hotelRepository.SelectAllDomainIds().ToList();
    }

    public HotelDto View(Guid hotelId)
    {
      var hotel = ServiceUtils.ResolveHotel(hotelId, _hotelRepository);
      return hotel.ToDto();
    }

    public Guid Create(string name, string descriotion, string imageUrl, int category)
    {
      _hotelRepository.StartTransaction();

      var hotel = new Hotel(Guid.NewGuid(), name, descriotion, category);

      _hotelRepository.Add(hotel);
      _hotelRepository.Commit();

      return hotel.DomainId;
    }

    public void AddService(Guid hotelId, Guid serviceId)
    {
      _hotelRepository.StartTransaction();

      var hotel = ServiceUtils.ResolveHotel(hotelId, _hotelRepository);
      var service = ServiceUtils.ResolveService(serviceId, _serviceRepository);

      hotel.AddService(service);

      _hotelRepository.Commit();
    }
  }
}
