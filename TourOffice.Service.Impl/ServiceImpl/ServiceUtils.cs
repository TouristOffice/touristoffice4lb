﻿using System;
using TourOffice.Model.Mod;
using TourOffice.Repository;
using TourOffice.Repository.Mod;
using TourOffice.Utils;

namespace TourOffice.Service.Impl.ServiceImpl
{
  static class ServiceUtils
  {
    private static TEntity ResolveEntity<TEntity> (
        IRepository<TEntity> repository,
        Guid domainId
    ) where TEntity : Entity
    {
      var entity = repository.FindByDomainId(domainId);
      if (entity != null)
        return entity;

      throw new ArgumentException("Unresolved entity");
    }

    public static Account ResolveAccount (
      Guid accountId, IAccountRepository accountRepository)
    {
      return ResolveEntity(accountRepository, accountId);
    }

    public static Country ResolveCountry (
      Guid countryId, ICountryRepository countryRepository)
    {
      return ResolveEntity(countryRepository, countryId);
    }

    public static Region ResolveRegion (
      Guid regionId, IRegionRepository regionRepository)
    {
      return ResolveEntity(regionRepository, regionId);
    }

    public static Hotel ResolveHotel (
      Guid hotelId, IHotelRepository hotelRepository)
    {
      return ResolveEntity(hotelRepository, hotelId);
    }

    public static Tour ResolveTour (
      Guid tourId, ITourRepository tourRepository)
    {
      return ResolveEntity(tourRepository, tourId);
    }

    public static TourItem ResolveTourItem (
      Guid tourItemId, ITourItemRepository tourItemRepository)
    {
      return ResolveEntity(tourItemRepository, tourItemId);
    }

    public static AssignmentTour ResolveAssignmentTour (
      Guid assigmentTourId, IAssignmentTourRepository assignmentTourRepository)
    {
      return ResolveEntity(assignmentTourRepository, assigmentTourId);
    }

    public static Order ResolveOrder (
      Guid orderId, IOrderRepository orderRepository)
    {
      return ResolveEntity(orderRepository, orderId);
    }

    public static Model.Mod.Service ResolveService (
      Guid serviceId, IServiceRepository serviceRepository)
    {
      return ResolveEntity(serviceRepository, serviceId);
    }
  }
}
