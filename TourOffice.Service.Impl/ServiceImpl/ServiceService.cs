﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Dto.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Service.Impl.ServiceImpl
{
  public class ServiceService : IServiceService
  {
    private readonly IServiceRepository _serviceRepository;

    public ServiceService(IServiceRepository serviceRepository)
    {
      _serviceRepository = serviceRepository;
    }

    public IList<Guid> ViewAll()
    {
      return _serviceRepository.SelectAllDomainIds().ToList();
    }

    public ServiceDto View(Guid serviceId)
    {
      var service = ServiceUtils.ResolveService(serviceId, _serviceRepository);
      return service.ToDto();
    }

    public Guid Create(string name)
    {
      _serviceRepository.StartTransaction();

      var service = new Model.Mod.Service(Guid.NewGuid(), name);

      _serviceRepository.Add(service);
      _serviceRepository.Commit();

      return service.DomainId;
    }
  }
}
