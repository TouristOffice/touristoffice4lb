﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Dto.Mod;
using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Service.Impl.ServiceImpl
{
  public class AssignmentTourService : IAssignmentTourService
  {
    private readonly IAssignmentTourRepository _assignmentTourRepository;
    private readonly ITourItemRepository _tourItemRepository;
    private readonly IHotelRepository _hotelRepository;

    public AssignmentTourService(
      IAssignmentTourRepository assignmentTourRepository,
      ITourItemRepository tourItemRepository,
      IHotelRepository hotelRepository
      )
    {
      _assignmentTourRepository = assignmentTourRepository;
      _tourItemRepository = tourItemRepository;
      _hotelRepository = hotelRepository;
    }

    public IList<Guid> ViewAll()
    {
      return _assignmentTourRepository.SelectAllDomainIds().ToList();
    }

    public AssignmentTourDto View(Guid assignmentTourId)
    {
      var assignmenTour = ServiceUtils.ResolveAssignmentTour(assignmentTourId, _assignmentTourRepository);
      return assignmenTour.ToDto();
    }

    public Guid Create(Guid tourItemId, Guid hotelId)
    {
      _assignmentTourRepository.StartTransaction();

      var tourItem = ServiceUtils.ResolveTourItem(tourItemId, _tourItemRepository);
      var hotel = ServiceUtils.ResolveHotel(hotelId, _hotelRepository);

      var assignmentTour = new AssignmentTour(Guid.NewGuid(), hotel, tourItem);

      _assignmentTourRepository.Add(assignmentTour);
      _assignmentTourRepository.Commit();

      return assignmentTour.DomainId;
    }
  }
}
