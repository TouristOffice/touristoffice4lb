﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Dto.Mod;
using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Service.Impl.ServiceImpl
{
  public class RegionService : IRegionService
  {
    private readonly IRegionRepository _regionRepository;
    private readonly ITourRepository _tourRepository;

    public RegionService(
      IRegionRepository regionRepository, 
      ITourRepository tourRepository)
    {
      _regionRepository = regionRepository;
      _tourRepository = tourRepository;
    }

    public IList<Guid> ViewAll()
    {
      return _regionRepository.SelectAllDomainIds().ToList();
    }

    public RegionDto View(Guid regionId)
    {
      var region = ServiceUtils.ResolveRegion(regionId, _regionRepository);
      return region.ToDto();
    }

    public Guid Create(string name)
    {
      _regionRepository.StartTransaction();

      var region = new Region(Guid.NewGuid(), name);

      _regionRepository.Add(region);
      _regionRepository.Commit();

      return region.DomainId;
    }

    public void AddTour(Guid regionId, Guid tourId)
    {
      _regionRepository.StartTransaction();

      var region = ServiceUtils.ResolveRegion(regionId, _regionRepository);
      var tour = ServiceUtils.ResolveTour(tourId, _tourRepository);

      region.AddTour(tour);

      _regionRepository.Commit();
    }
  }
}
