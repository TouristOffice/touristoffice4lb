﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Dto.Mod;
using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Service.Impl.ServiceImpl
{
  public class OrderService : IOrderService
  {
    private readonly IOrderRepository _orderRepository;
    private readonly IAssignmentTourRepository _assignmentTourRepository;

    public OrderService (
      IOrderRepository orderRepository, 
      IAssignmentTourRepository assignmentTourRepository
      )
    {
      _orderRepository = orderRepository;
      _assignmentTourRepository = assignmentTourRepository;
    }

    public IList<Guid> ViewAll ()
    {
      return _orderRepository.SelectAllDomainIds().ToList();
    }

    public IList<Guid> ViewUnconfirmed ()
    {
      return _orderRepository.SelectUnconfirmedIds().ToList();
    }

    public IList<Guid> ViewReady4Delivery ()
    {
      return _orderRepository.SelectReady4DeliveryIds().ToList();
    }

    public OrderDto View (Guid orderId)
    {
      var o = ServiceUtils.ResolveOrder(orderId, _orderRepository);
      return o.ToDto();
    }

    public Guid CreateNew (Guid assignmentTourId)
    {
      _orderRepository.StartTransaction();

      var assignmentTour = ServiceUtils.ResolveAssignmentTour(assignmentTourId, _assignmentTourRepository);
      var o = new Order(Guid.NewGuid(), DateTime.Now, assignmentTour);

      _orderRepository.Add(o);

      _orderRepository.Commit();

      return o.DomainId;
    }

    public void SetDiscount (Guid orderId, decimal discountPercent)
    {
      _orderRepository.StartTransaction();

      var o = ServiceUtils.ResolveOrder(orderId, _orderRepository);
      o.SetDiscount(new Discount(discountPercent));

      _orderRepository.Commit();
    }

    public void Confirm (Guid orderId)
    {
      _orderRepository.StartTransaction();

      var o = ServiceUtils.ResolveOrder(orderId, _orderRepository);
      o.Confirm();

      _orderRepository.Commit();

      //cookingAssignmentRepository.StartTransaction();

      //foreach (var ca in o.GenerateCookingAssignments())
      //  cookingAssignmentRepository.Add(ca);

      //cookingAssignmentRepository.Commit();
    }

    public void Cancel (Guid orderId)
    {
      _orderRepository.StartTransaction();

      var o = ServiceUtils.ResolveOrder(orderId, _orderRepository);
      o.Cancel();

      _orderRepository.Commit();
    }
  }
}
