﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Dto.Mod;
using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Service.Impl.ServiceImpl
{
  public class TourService : ITourService
  {
    private readonly ITourRepository _tourRepository;
    private readonly ITourItemRepository _tourItemRepository;
    private readonly IHotelRepository _hotelRepository;

    public TourService(
      ITourRepository tourRepository, 
      ITourItemRepository tourItemRepository,
      IHotelRepository hotelRepository)
    {
      _tourRepository = tourRepository;
      _tourItemRepository = tourItemRepository;
      _hotelRepository = hotelRepository;
    }

    public IList<Guid> ViewAll()
    {
      return _tourRepository.SelectAllDomainIds().ToList();
    }

    public TourDto View(Guid tourId)
    {
      var tour = ServiceUtils.ResolveTour(tourId, _tourRepository);
      return tour.ToDto();
    }

    public Guid Create(string name, Guid hotelId)
    {
      _tourRepository.StartTransaction();

      var hotel = ServiceUtils.ResolveHotel(hotelId, _hotelRepository);

      var newTour = new Tour(Guid.NewGuid(), name, hotel);

      _tourRepository.Add(newTour);
      _tourRepository.Commit();

      return newTour.DomainId;
    }

    public void AddTourItem(Guid tourId, Guid tourItemId)
    {
      _tourRepository.StartTransaction();

      var tour = ServiceUtils.ResolveTour(tourId, _tourRepository);
      var tourItem = ServiceUtils.ResolveTourItem(tourItemId, _tourItemRepository);

      tour.AddTourItem(tourItem);
      _tourRepository.Commit();
    }
  }
}
