﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Dto.Mod;
using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Service.Impl.ServiceImpl
{
  public class TourItemService : ITourItemService
  {
    private readonly ITourItemRepository _tourItemRepository;

    public TourItemService(ITourItemRepository tourItemRepository)
    {
      _tourItemRepository = tourItemRepository;
    }

    public IList<Guid> ViewAll()
    {
      return _tourItemRepository.SelectAllDomainIds().ToList();
    }

    public TourItemDto View(Guid tourItemId)
    {
      var tourItem = ServiceUtils.ResolveTourItem(tourItemId, _tourItemRepository);
      return tourItem.ToDto();
    }

    public Guid Create(int quantityNight, DateTime departureDate, decimal price)
    {
      _tourItemRepository.StartTransaction();

      var newTourItem = new TourItem(Guid.NewGuid(), quantityNight, departureDate, price);

      _tourItemRepository.Add(newTourItem);
      _tourItemRepository.Commit();

      return newTourItem.DomainId;
    }
  }
}
