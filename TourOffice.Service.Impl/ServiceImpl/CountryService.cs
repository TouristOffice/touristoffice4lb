﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Dto.Mod;
using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Service.Impl.ServiceImpl
{
  public class CountryService : ICountryService
  {
    private readonly ICountryRepository _countryRepository;
    private readonly IRegionRepository _regionRepository;

    public CountryService(ICountryRepository countryRepository, IRegionRepository regionRepository)
    {
      _countryRepository = countryRepository;
      _regionRepository = regionRepository;
    }

    public IList<Guid> ViewAll()
    {
      return _countryRepository.SelectAllDomainIds().ToList();
    }

    public CountryDto View(Guid domainId)
    {
      var country = ServiceUtils.ResolveCountry(domainId, _countryRepository);
      return country.ToDto();
    }

    public Guid Create(string name, string about)
    {
      _countryRepository.StartTransaction();

      var country = new Country(Guid.NewGuid(), name, about);

      _countryRepository.Add(country);
      _countryRepository.Commit();

      return country.DomainId;
    }

    public void AddRegion(Guid countryId, Guid regionId)
    {
      _countryRepository.StartTransaction();

      var country = ServiceUtils.ResolveCountry(countryId, _countryRepository);
      var region = ServiceUtils.ResolveRegion(regionId, _regionRepository);

      country.AddRegion(region);
      _countryRepository.Commit();
    }
  }
}
