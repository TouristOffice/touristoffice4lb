﻿using TourOffice.Repository.Mod;
using TourOffice.Service.Impl.ServiceImpl;

namespace TourOffice.Service.Impl
{
  public static class ServiceFactory
  {
    public static IAccountService MakeAccountService (
                IAccountRepository accountRepository,
                IOrderRepository orderRepository)
    {
      return new AccountService(accountRepository, orderRepository);
    }

    public static IOrderService MakeOrderService (
      IOrderRepository orderRepository, 
      IAssignmentTourRepository assignmentTourRepository)
    {
      return new OrderService(orderRepository, assignmentTourRepository);
    }

    public static ICountryService MakeCountryService (
      ICountryRepository countryRepository, IRegionRepository regionRepository)
    {
      return new CountryService(countryRepository, regionRepository);
    }

    public static IRegionService MakeRegionService (
      IRegionRepository regionRepository,
      ITourRepository tourRepository)
    {
      return new RegionService(regionRepository, tourRepository);
    }

    public static IHotelService MakeHotelService (
      IHotelRepository hotelRepository,
      IServiceRepository serviceRepository)
    {
      return new HotelService(hotelRepository, serviceRepository);
    }

    public static IServiceService MakeServiceService (IServiceRepository serviceRepository)
    {
      return new ServiceService(serviceRepository);
    }

    public static ITourService MakeTourService (
      ITourRepository tourRepository,
      ITourItemRepository tourItemRepository,
      IHotelRepository hotelRepository)
    {
      return new TourService(tourRepository, tourItemRepository, hotelRepository);
    }

    public static ITourItemService MakeTourItemService (ITourItemRepository tourItemRepository)
    {
      return new TourItemService(tourItemRepository);
    }

    public static IAssignmentTourService MakeAssignmentTourService (
      IAssignmentTourRepository assignmentTourRepository,
      ITourItemRepository tourItemRepository,
      IHotelRepository hotelRepository)
    {
      return new AssignmentTourService(assignmentTourRepository, tourItemRepository, hotelRepository);
    }
  }
}
