﻿using System.Collections.Generic;
using TourOffice.Utils;
using TourOffice.Utils.Property;

namespace TourOffice.Model.Mod
{
  public class Contact : Value<Contact>
  {
    private readonly NonEmptyString _address = new NonEmptyString("address");
    private readonly NonEmptyString _phone = new NonEmptyString("phone");

    public string Address
    {
      get { return _address.Value; }
      private set { _address.Value = value; }
    }

    public string Phone
    {
      get { return _phone.Value; }
      private set { _phone.Value = value; }
    }

    protected Contact () { }

    public Contact (string address, string phone)
    {
      _address.Value = address;
      _phone.Value = phone;
    }

    protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck ()
    {
      return new object[] { Address, Phone };
    }
  }
}
