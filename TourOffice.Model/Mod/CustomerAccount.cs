﻿using System;
using System.Collections.Generic;
using TourOffice.Utils.Property;

namespace TourOffice.Model.Mod
{
  public class CustomerAccount : Account
  {
    private readonly RequiredProperty<Contact> _contact = new RequiredProperty<Contact>("contact");

    public virtual ICollection<Order> History { get; private set; } = new List<Order>();

    public virtual Contact Contact
    {
      get { return _contact.Value; }
      set { _contact.Value = value; }
    }

    protected CustomerAccount () { }

    public CustomerAccount (Guid domainId, string name, string email, string passwordHash, Contact contact)
        : base(domainId, name, email, passwordHash)
    {
      _contact.Value = contact;
    }

    public override void Accept (IAccountVisitor visitor)
    {
      visitor.Visit(this);
    }
  }
}
