﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Utils;
using TourOffice.Utils.Property;

namespace TourOffice.Model.Mod
{
  public class Region : Entity
  {
    private readonly NonEmptyString _name = new NonEmptyString("name");

    public virtual ICollection<Tour> ListTour { get; } = new List<Tour>();

    public string Name
    {
      get { return _name.Value; }
      set { _name.Value = value; }
    }

    protected Region () { }

    public Region (Guid domainId, string name)
        : base(domainId)
    {
      _name.Value = name;
    }

    public void AddTour (Tour tour)
    {
      var ti = ListTour.FirstOrDefault(t => t.DomainId == tour.DomainId);

      if (ti != null)
        throw new ArgumentException("Is already tour item");

      ListTour.Add(tour);
    }

    public override string ToString ()
    {
      return string.Format(
                 "ID = {0}\nName = {1}\n",
                 DomainId,
                 Name
             );
    }
  }
}
