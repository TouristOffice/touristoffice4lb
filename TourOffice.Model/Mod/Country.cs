﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Utils;
using TourOffice.Utils.Property;

namespace TourOffice.Model.Mod
{
  public class Country : Entity
  {
    private readonly NonEmptyString _name = new NonEmptyString("name");

    public string About { get; private set; }

    public virtual ICollection<Region> ListRegion { get; private set; } = new List<Region>();

    public string Name
    {
      get { return _name.Value; }
      set { _name.Value = value; }
    }

    protected Country () { }

    public Country (Guid domainId, string name, string about)
        : base(domainId)
    {
      _name.Value = name;
      About = about;
    }

    public void AddRegion (Region region)
    {
      var r = ListRegion.FirstOrDefault(t => t.DomainId == region.DomainId);

      if (r != null)
        throw new ArgumentException("Is already tour item");

      ListRegion.Add(region);
    }

    public override string ToString ()
    {
      return string.Format(
                 "Id = {0}\nName = {1}\nAbout = {2}\n",
                 DomainId,
                 Name,
                 About
             );
    }
  }
}
