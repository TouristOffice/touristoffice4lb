﻿using System;
using TourOffice.Utils;
using TourOffice.Utils.Property;

namespace TourOffice.Model.Mod
{
  public class Account : Entity
  {
    private readonly NonEmptyString _name = new NonEmptyString("name");
    private readonly RegexString _email = new RegexString("email", "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");
    private readonly NonEmptyString _password = new NonEmptyString("password");

    public string Name
    {
      get { return _name.Value; }
      set { _name.Value = value; }
    }

    public string Email
    {
      get { return _email.Value; }
      set { _email.Value = value; }
    }

    public string Password
    {
      get { return _password.Value; }
      set { _password.Value = value; }
    }

    protected Account () { }

    public Account (Guid domainId, string name, string email, string password)
        : base(domainId)
    {
      Name = name;
      Email = email;
      Password = password;
    }

    public bool CheckPassword (string password)
    {
      if (password == null)
        throw new ArgumentNullException(nameof(password));

      return Password == password;
    }

    public virtual void Accept (IAccountVisitor visitor)
    {
      visitor.Visit(this);
    }
  }
}
