﻿using System;
using TourOffice.Utils;
using TourOffice.Utils.Property;

namespace TourOffice.Model.Mod
{
  public class TourItem : Entity
  {
    private readonly RangeProperty<int> _quantityNight =
        new RangeProperty<int>("quantity", 0, false, int.MaxValue, true);

    private readonly RangeProperty<decimal> _price =
        new RangeProperty<decimal>("fixedPrice", 0, true, decimal.MaxValue, true);

    public DateTime DepartureDate { get; private set; }

    public int QuantityNight
    {
      get { return _quantityNight.Value; }
      private set { _quantityNight.Value = value; }
    }

    public decimal Price
    {
      get { return _price.Value; }
      private set { _price.Value = value; }
    }

    protected TourItem () { }

    public TourItem (Guid domainId, int quantityNight, DateTime departureDate, decimal price)
      :base(domainId)
    {
      if (quantityNight <= 0)
        throw new Exception("TourItem: non-positive quantity");

      _price.Value = price;
      _quantityNight.Value = quantityNight;
      DepartureDate = departureDate;
    }

    public override string ToString ()
    {
      return string.Format(
                 "Departure date = {0}, Quantity night = {1}, Price = {2}",
                 DepartureDate,
                 QuantityNight,
                 Price
             );
    }
  }
}
