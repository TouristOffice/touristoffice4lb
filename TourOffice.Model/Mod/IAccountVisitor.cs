﻿namespace TourOffice.Model.Mod
{
  public interface IAccountVisitor
  {
    void Visit (Account account);
    void Visit (OperatorAccount account);
    void Visit (CustomerAccount account);
  }
}
