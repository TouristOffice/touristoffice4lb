﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Utils;
using TourOffice.Utils.Property;

namespace TourOffice.Model.Mod
{
  public class Hotel : Entity
  {
    private readonly NonEmptyString _name = new NonEmptyString("name");
    private readonly NonEmptyString _description = new NonEmptyString("description");
    private readonly RangeProperty<int> _category = new RangeProperty<int>("category", 0, false, 5, true);

    public virtual ICollection<Service> ListService { get; private set; } = new List<Service>();

    public string ImageUrl { get; set; }

    public string Name
    {
      get { return _name.Value; }
      set { _name.Value = value; }
    }

    public int Category
    {
      get { return _category.Value; }
      set { _category.Value = value; }
    }

    public string Description
    {
      get { return _description.Value; }
      set { _description.Value = value; }
    }

    protected Hotel () { }

    public Hotel (Guid domainId, string name, string description, int category)
        : base(domainId)
    {
      _name.Value = name;
      _description.Value = description;
      _category.Value = category;
      ImageUrl = "no image";
    }

    public void AddService(Service service)
    {
      var t = ListService.FirstOrDefault(s => s.Name == service.Name);

      if (t != null)
        throw new ArgumentException("service already available");

      ListService.Add(service);
    }

    public override string ToString ()
    {
      return string.Format(
                 "Id = {0}\nName = {1}\nCategory = {2}\nImageUrl = {3}\nDescription = {4}",
                 DomainId,
                 Name,
                 Category,
                 ImageUrl,
                 Description
             );
    }
  }
}
