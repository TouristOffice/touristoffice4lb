﻿using System;
using TourOffice.Utils;
using TourOffice.Utils.Property;

namespace TourOffice.Model.Mod
{
  public class AssignmentTour : Entity
  {
    private readonly RequiredProperty<Hotel> _hotel = new RequiredProperty<Hotel>("hotel");
    private readonly RequiredProperty<TourItem> _tourItem = new RequiredProperty<TourItem>("tourItem");

    public virtual Hotel AssignedHotel
    {
      get { return _hotel.Value; }
      private set { _hotel.Value = value; }
    }

    public virtual TourItem AssignedItemTour
    {
      get { return _tourItem.Value; }
      private set { _tourItem.Value = value; }
    }

    protected AssignmentTour () { }

    public AssignmentTour (Guid domainId, Hotel hotel, TourItem tourItem)
        : base(domainId)
    {
      _hotel.Value = hotel;
      _tourItem.Value = tourItem;
    }

    public override string ToString ()
    {
      return string.Format(
                 "Id = {0}\nHotel = {1}\n Item tour:\n{2}",
                 DomainId,
                 AssignedHotel.Name,
                 AssignedItemTour
             );
    }
  }
}
