﻿using System;
using System.Collections.Generic;
using System.Linq;
using TourOffice.Utils;
using TourOffice.Utils.Property;

namespace TourOffice.Model.Mod
{
  public class Tour : Entity
  {
    private readonly NonEmptyString _name = new NonEmptyString("name");

    public virtual Hotel Hotel { get; set; }
    public virtual ICollection<TourItem> TourItems { get; } = new List<TourItem>();

    public string Name
    {
      get { return _name.Value; }
      set { _name.Value = value; }
    }

    protected Tour () { }

    public Tour (Guid domainId, string name, Hotel hotel)
        : base(domainId)
    {
      Name = name;
      Hotel = hotel;
    }

    public void AddTourItem(TourItem tourItem)
    {
      var tour = TourItems.FirstOrDefault(t => t.DomainId == tourItem.DomainId);

      if (tour != null)
        throw new ArgumentException("Is already tour item");

      TourItems.Add(tourItem);
    }

    public override string ToString ()
    {
      return string.Format(
                 "Id = {0}\nName = {1}",
                 DomainId,
                 Name
             );
    }
  }
}
