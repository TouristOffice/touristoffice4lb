﻿using System;
using System.Collections.Generic;

namespace TourOffice.Model.Mod
{
  public class OperatorAccount : Account
  {
    public virtual ICollection<Order> Orders { get; private set; }

    protected OperatorAccount () { }

    public OperatorAccount (Guid domainId, string name, string email, string passwordHash)
        : base(domainId, name, email, passwordHash)
    {
      Orders = new List<Order>();
    }

    public void TrackOrder (Order order)
    {
      if (order == null)
        throw new ArgumentNullException("order");

      Orders.Add(order);
    }

    public override void Accept (IAccountVisitor visitor)
    {
      visitor.Visit(this);
    }
  }
}
