﻿using System;
using TourOffice.Utils;
using TourOffice.Utils.Property;

namespace TourOffice.Model.Mod
{
  public class Service : Entity
  {
    private readonly NonEmptyString _name = new NonEmptyString("name");

    public string Name
    {
      get { return _name.Value; }
      set { _name.Value = value; }
    }

    protected Service () { }

    public Service (Guid domainId, string name) : base(domainId)
    {
      _name.Value = name;
    }

    public override string ToString ()
    {
      return _name.Value;
    }
  }
}
