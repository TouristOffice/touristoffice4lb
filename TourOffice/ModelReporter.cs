﻿using System.IO;
using TourOffice.Dto;
using TourOffice.Service;

namespace TourOffice
{
  class ModelReporter
  {
    private readonly ServiceProvider _serviceProvider;
    private readonly TextWriter _output;

    public ModelReporter (ServiceProvider serviceProvider, TextWriter output)
    {
      _serviceProvider = serviceProvider;
      _output = output;
    }

    public void GenerateReport ()
    {
      ReportCollection("Accounts", _serviceProvider.ProvideAccountService());
      ReportCollection("Orders", _serviceProvider.ProvideOrderService());
      ReportCollection("Countries", _serviceProvider.ProvideCountryService());
      ReportCollection("Regions", _serviceProvider.ProvideRegionService());
      ReportCollection("Hotels", _serviceProvider.ProvideHotelService());
      ReportCollection("Assignment Tours", _serviceProvider.ProvideAssignmentTourService());
      ReportCollection("Tours", _serviceProvider.ProvideTourService());
      ReportCollection("TourItems", _serviceProvider.ProvideTourItemService());
      ReportCollection("Services", _serviceProvider.ProvideServiceService());
    }

    private void ReportCollection<TDto> (string title, IDomainEntityService<TDto> service)
            where TDto : DomainEntityDto<TDto>
    {
      _output.WriteLine("==== {0} ==== ", title);
      _output.WriteLine();

      foreach (var entityId in service.ViewAll())
      {
        _output.Write(service.View(entityId));

        _output.WriteLine();
        _output.WriteLine();
      }

      _output.WriteLine();
    }
  }
}
