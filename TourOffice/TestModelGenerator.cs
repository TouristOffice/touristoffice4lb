﻿using System;

namespace TourOffice
{
  class TestModelGenerator
  {
    private readonly ServiceProvider _serviceProvider;

    private Guid _order;
    private Guid _country;
    private Guid _region;
    private Guid _hotel;
    private Guid _tour;
    private Guid _miniBar,_food,_restoran,_safe;
    private Guid _tourItem1, _tourItem2, _tourItem3;
    private Guid _assignmentTour;

    public TestModelGenerator (ServiceProvider serviceProvider)
    {
      _serviceProvider = serviceProvider;
    }

    public void GenerateTestData ()
    {
      GenerateServices();
      GenerateTourItems();
      GenerateHotels();
      GenerateTours();
      GenerateRegions();
      GenerateCountries();
      GenerateAssignmentTours();
      GenerateOrders();
      GenerateAccounts();
    }

    private void GenerateAccounts ()
    {
      var service = _serviceProvider.ProvideAccountService();

      var lusya = service.CreateOperator("Lusya Petrova", "lusya.gorna@tour.com", "qwerty");

      service.TrackOrder(lusya, _order);
    }

    private void GenerateCountries ()
    {
      var service = _serviceProvider.ProvideCountryService();

      _country = service.Create("Australia", "About country");

      service.AddRegion(_country, _region);
    }

    private void GenerateRegions ()
    {
      var service = _serviceProvider.ProvideRegionService();

      _region = service.Create("Vena");

      service.AddTour(_region, _tour);
    }

    private void GenerateServices ()
    {
      var service = _serviceProvider.ProvideServiceService();

      _miniBar   = service.Create("mini-bar");
      _food      = service.Create("food");
      _restoran  = service.Create("restoran");
      _safe      = service.Create("safe");
    }

    private void GenerateHotels ()
    {
      var service = _serviceProvider.ProvideHotelService();

      _hotel = service.Create("Ogor", "description...", 
        "http://EndsOfTheEarth.com/uploads/img/1280x620/ogor.jpg", 5);

      service.AddService(_hotel, _miniBar);
      service.AddService(_hotel, _food);
      service.AddService(_hotel, _restoran);
      service.AddService(_hotel, _safe);
    }

    private void GenerateTourItems ()
    {
      var service = _serviceProvider.ProvideTourItemService();

      _tourItem1 = service.Create(5, new DateTime(2016, 6, 12), 300);
      _tourItem2 = service.Create(7, new DateTime(2016, 5, 5), 250);
      _tourItem3 = service.Create(9, new DateTime(2016, 6, 12), 300);
    }

    private void GenerateTours ()
    {
      var service = _serviceProvider.ProvideTourService();

      _tour = service.Create("Roged", _hotel);

      service.AddTourItem(_tour, _tourItem1);
      service.AddTourItem(_tour, _tourItem2);
      service.AddTourItem(_tour, _tourItem3);
    }

    private void GenerateAssignmentTours ()
    {
      var service = _serviceProvider.ProvideAssignmentTourService();

      _assignmentTour = service.Create(_tourItem1, _hotel);
    }

    private void GenerateOrders ()
    {
      var service = _serviceProvider.ProvideOrderService();

      _order = service.CreateNew(_assignmentTour);
      service.SetDiscount(_order, 20.00M);
    }
  }
}
