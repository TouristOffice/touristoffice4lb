﻿using System;
using TourOffice.Repository.EntityFramework;

namespace TourOffice
{
  class Program
  {
    static void Main()
    {
      try
      {
        using (var dbContext = new TourOfficeDbContext())
        {
          var serviceProvider = new ServiceProvider(dbContext);

          var generator = new TestModelGenerator(serviceProvider);
          generator.GenerateTestData();
        }

        using (var dbContext = new TourOfficeDbContext())
        {
          var serviceProvider = new ServiceProvider(dbContext);

          var reportGenerator = new ModelReporter(serviceProvider, Console.Out);
          reportGenerator.GenerateReport();
        }
      }
      catch (Exception e)
      {
        Console.WriteLine(e.GetType().FullName);
        Console.WriteLine(e.Message);
        Console.WriteLine(e.StackTrace);
      }
    }
  }
}
