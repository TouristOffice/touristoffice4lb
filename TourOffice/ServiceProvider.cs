﻿using TourOffice.Repository.EntityFramework;
using TourOffice.Service;
using TourOffice.Service.Impl;

namespace TourOffice
{
  public class ServiceProvider
  {
    private readonly TourOfficeDbContext _dbContext;

    public ServiceProvider (TourOfficeDbContext dbContext)
    {
      _dbContext = dbContext;
    }

    public IAccountService ProvideAccountService ()
    {
      var accountRepository = RepositoryFactory.MakeAccountRepository(_dbContext);
      var orderRepository = RepositoryFactory.MakeOrderRepository(_dbContext);

      return ServiceFactory.MakeAccountService(
              accountRepository,
              orderRepository
      );
    }

    public IOrderService ProvideOrderService ()
    {
      var orderRepository = RepositoryFactory.MakeOrderRepository(_dbContext);
      var assignmentTourRepository = RepositoryFactory.MakeAssignmentTourRepository(_dbContext);

      return ServiceFactory.MakeOrderService(orderRepository, assignmentTourRepository);
    }

    public ICountryService ProvideCountryService()
    {
      var countryRepository = RepositoryFactory.MakeCountryRepository(_dbContext);
      var regionRepository = RepositoryFactory.MakeRegionRepository(_dbContext);

      return ServiceFactory.MakeCountryService(countryRepository, regionRepository);
    }

    public IRegionService ProvideRegionService ()
    {
      var regionRepository = RepositoryFactory.MakeRegionRepository(_dbContext);
      var tourRepository = RepositoryFactory.MakeTourRepository(_dbContext);

      return ServiceFactory.MakeRegionService(regionRepository, tourRepository);
    }

    public IHotelService ProvideHotelService ()
    {
      var hotelRepository = RepositoryFactory.MakeHotelRepository(_dbContext);
      var serviceRepository = RepositoryFactory.MakeServiceRepository(_dbContext);

      return ServiceFactory.MakeHotelService(hotelRepository, serviceRepository);
    }

    public IServiceService ProvideServiceService ()
    {
      var serviceRepository = RepositoryFactory.MakeServiceRepository(_dbContext);

      return ServiceFactory.MakeServiceService(serviceRepository);
    }

    public IAssignmentTourService ProvideAssignmentTourService ()
    {
      var assignmentTourRepository = RepositoryFactory.MakeAssignmentTourRepository(_dbContext);
      var tourItemRepository = RepositoryFactory.MakeTourItemRepository(_dbContext);
      var hotelRepository = RepositoryFactory.MakeHotelRepository(_dbContext);

      return ServiceFactory.MakeAssignmentTourService(assignmentTourRepository, tourItemRepository, hotelRepository);
    }

    public ITourService ProvideTourService ()
    {
      var tourRepository = RepositoryFactory.MakeTourRepository(_dbContext);
      var tourItemRepository = RepositoryFactory.MakeTourItemRepository(_dbContext);
      var hotelRepository = RepositoryFactory.MakeHotelRepository(_dbContext);

      return ServiceFactory.MakeTourService(tourRepository, tourItemRepository, hotelRepository);
    }

    public ITourItemService ProvideTourItemService ()
    {
      var tourItemRepository = RepositoryFactory.MakeTourItemRepository(_dbContext);

      return ServiceFactory.MakeTourItemService(tourItemRepository);
    }
  }
}
