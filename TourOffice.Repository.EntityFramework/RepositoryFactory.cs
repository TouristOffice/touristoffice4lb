﻿using TourOffice.Repository.EntityFramework.RepositoryImplementations;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework
{
  public static class RepositoryFactory
  {
    public static IAccountRepository MakeAccountRepository (TourOfficeDbContext dbContext)
    {
      return new AccountRepository(dbContext);
    }

    public static IOperatorAccountRepository MakeOperatorAccountRepository (TourOfficeDbContext dbContext)
    {
      return new OperatorAccountRepository(dbContext);
    }

    public static ICustomerAccountRepository MakeCustomerAccountRepository (TourOfficeDbContext dbContext)
    {
      return new CustomerAccountRepository(dbContext);
    }

    public static ICountryRepository MakeCountryRepository (TourOfficeDbContext dbContext)
    {
      return new CountryRepository(dbContext);
    }

    public static IRegionRepository MakeRegionRepository (TourOfficeDbContext dbContext)
    {
      return new RegionRepository(dbContext);
    }

    public static IHotelRepository MakeHotelRepository (TourOfficeDbContext dbContext)
    {
      return new HotelRepository(dbContext);
    }

    public static ITourRepository MakeTourRepository (TourOfficeDbContext dbContext)
    {
      return new TourRepository(dbContext);
    }

    public static ITourItemRepository MakeTourItemRepository (TourOfficeDbContext dbContext)
    {
      return new TourItemRepository(dbContext);
    }

    public static IAssignmentTourRepository MakeAssignmentTourRepository (TourOfficeDbContext dbContext)
    {
      return new AssignmentTourRepository(dbContext);
    }

    public static IOrderRepository MakeOrderRepository (TourOfficeDbContext dbContext)
    {
      return new OrderRepository(dbContext);
    }

    public static IServiceRepository MakeServiceRepository (TourOfficeDbContext dbContext)
    {
      return new ServiceRepository(dbContext);
    }
  }
}
