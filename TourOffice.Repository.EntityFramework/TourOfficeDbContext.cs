﻿using System.Data.Entity;
using System.Diagnostics;
using TourOffice.Model.Mod;
using TourOffice.Repository.EntityFramework.Configurations;

namespace TourOffice.Repository.EntityFramework
{
  public class TourOfficeDbContext : DbContext
  {
    public DbSet<CustomerAccount> CastomerAccounts { get; set; }
    public DbSet<OperatorAccount> OperatorAccounts { get; set; }
    public DbSet<Account> Accounts { get; set; }
    public DbSet<Country> Countrys { get; set; }
    public DbSet<Region> Regions { get; set; }
    public DbSet<Hotel> Hotels { get; set; }
    public DbSet<Tour> Tours { get; set; }
    public DbSet<AssignmentTour> AssignmentTours { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<Service> Services { get; set; } 
    public DbSet<TourItem> TourItems { get; set; }

    static TourOfficeDbContext()
    {
      Database.SetInitializer(new DropCreateDatabaseAlways<TourOfficeDbContext>());
    }

    public TourOfficeDbContext()
    {
      Database.Log = (s => Debug.WriteLine(s));
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      modelBuilder.Configurations.Add(new CustomerAccountConfiguration());
      modelBuilder.Configurations.Add(new OperatorAccountConfiguration());
      modelBuilder.Configurations.Add(new AccountConfiguration());
      modelBuilder.Configurations.Add(new CountryConfiguration());
      modelBuilder.Configurations.Add(new RegionConfiguration());
      modelBuilder.Configurations.Add(new HotelConfiguration());
      modelBuilder.Configurations.Add(new TourConfiguration());
      modelBuilder.Configurations.Add(new OrderConfiguration());
      modelBuilder.Configurations.Add(new AssignmentTourConfiguration());
      modelBuilder.Configurations.Add(new ServiceConfiguration());

      modelBuilder.Configurations.Add(new DiscountConfiguration());
      modelBuilder.Configurations.Add(new TourItemConfiguration());
      modelBuilder.Configurations.Add(new ContactConfiguration());
    }
  }
}
