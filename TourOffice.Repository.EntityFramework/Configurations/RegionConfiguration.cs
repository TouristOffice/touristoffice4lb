﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class RegionConfiguration : BasicEntityConfiguration<Region>
  {
    public RegionConfiguration ()
    {
      Property(r => r.Name).IsRequired();
    }
  }
}
