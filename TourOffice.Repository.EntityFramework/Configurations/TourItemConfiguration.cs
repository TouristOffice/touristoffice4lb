﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class TourItemConfiguration : BasicEntityConfiguration<TourItem>
  {
    public TourItemConfiguration ()
    {
      Property(ti => ti.Price);
      Property(ti => ti.QuantityNight);
      Property(ti => ti.DepartureDate);
    }
  }
}
