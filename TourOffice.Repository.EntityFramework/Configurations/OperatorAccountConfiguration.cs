﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class OperatorAccountConfiguration : BasicEntityConfiguration<OperatorAccount>
  {
    public OperatorAccountConfiguration ()
    {
      Property(a => a.Name).IsRequired();
      Property(a => a.Email).IsRequired();
      Property(a => a.Password).IsRequired();

      HasMany(o => o.Orders).WithOptional();
    }
  }
}
