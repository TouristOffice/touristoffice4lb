﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class ServiceConfiguration : BasicEntityConfiguration<Service>
  {
    public ServiceConfiguration ()
    {
      Property(s => s.Name).IsRequired();
    }
  }
}
