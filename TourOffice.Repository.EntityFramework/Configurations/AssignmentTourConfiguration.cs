﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class AssignmentTourConfiguration : BasicEntityConfiguration<AssignmentTour>
  {
    public AssignmentTourConfiguration ()
    {
      HasRequired(a => a.AssignedHotel);
      HasRequired(a => a.AssignedItemTour);
    }
  }
}
