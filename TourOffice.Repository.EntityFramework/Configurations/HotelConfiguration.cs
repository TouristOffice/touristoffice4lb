﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class HotelConfiguration : BasicEntityConfiguration<Hotel>
  {
    public HotelConfiguration ()
    {
      Property(h => h.Name);
      Property(h => h.ImageUrl);
      Property(h => h.Description);
      Property(h => h.Category);

      HasMany(h => h.ListService).WithOptional();
    }
  }
}
