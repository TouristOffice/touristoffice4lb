﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class CustomerAccountConfiguration : BasicEntityConfiguration<CustomerAccount>
  {
    public CustomerAccountConfiguration ()
    {
      Property(a => a.Name).IsRequired();
      Property(a => a.Email).IsRequired();
      Property(a => a.Password).IsRequired();

      HasMany(c => c.History).WithOptional();
    }
  }
}
