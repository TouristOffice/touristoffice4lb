﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class TourConfiguration : BasicEntityConfiguration<Tour>
  {
    public TourConfiguration ()
    {
      Property(t => t.Name).IsRequired();
      HasRequired(t => t.Hotel);
      HasMany(t => t.TourItems).WithOptional();
    }
  }
}
