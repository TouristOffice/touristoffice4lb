﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class ContactConfiguration : BasicValueConfiguration<Contact>
  {
    public ContactConfiguration ()
    {
      Property(c => c.Address).IsRequired();
      Property(c => c.Phone).IsRequired();
    }
  }
}
