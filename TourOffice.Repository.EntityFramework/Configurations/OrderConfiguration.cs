﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class OrderConfiguration : BasicEntityConfiguration<Order>
  {
    public OrderConfiguration ()
    {
      HasRequired(o => o.AssignedTour);
    }
  }
}
