﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class AccountConfiguration : BasicEntityConfiguration<Account>
  {
    public AccountConfiguration()
    {
      Property(a => a.Name).IsRequired();
      Property(a => a.Email).IsRequired();
      Property(a => a.Password).IsRequired();
    }
  }
}
