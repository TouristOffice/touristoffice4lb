﻿using System.Data.Entity.ModelConfiguration;
using TourOffice.Utils;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  abstract class BasicEntityConfiguration<T> : EntityTypeConfiguration<T>
    where T : Entity
  {
    protected BasicEntityConfiguration ()
    {
      HasKey(e => e.DatabaseId);
      Property(e => e.DomainId).IsRequired();
    }
  }
}
