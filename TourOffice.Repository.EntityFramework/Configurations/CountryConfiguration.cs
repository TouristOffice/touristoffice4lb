﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class CountryConfiguration : BasicEntityConfiguration<Country>
  {
    public CountryConfiguration ()
    {
      Property(c => c.Name).IsRequired();
      Property(c => c.About).IsRequired();

      HasMany(c => c.ListRegion).WithOptional();
    }
  }
}
