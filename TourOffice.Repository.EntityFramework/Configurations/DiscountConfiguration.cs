﻿using TourOffice.Model.Mod;

namespace TourOffice.Repository.EntityFramework.Configurations
{
  class DiscountConfiguration : BasicValueConfiguration<Discount>
  {
    public DiscountConfiguration ()
    {
      Property(d => d.Percent).IsRequired();
    }
  }
}
