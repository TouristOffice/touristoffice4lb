﻿using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  class CountryRepository : BasicRepository<Country>, ICountryRepository
  {
    public CountryRepository (TourOfficeDbContext dbContext)
        : base(dbContext, dbContext.Countrys)
    {
    }
  }
}
