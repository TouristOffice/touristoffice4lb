﻿using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  class HotelRepository : BasicRepository<Hotel>, IHotelRepository
  {
    public HotelRepository (TourOfficeDbContext dbContext)
        : base(dbContext, dbContext.Hotels)
    {
    }
  }
}
