﻿using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  public class TourItemRepository : BasicRepository<TourItem>, ITourItemRepository
  {
    public TourItemRepository(TourOfficeDbContext dbContext) 
      : base(dbContext, dbContext.TourItems)
    {
    }
  }
}
