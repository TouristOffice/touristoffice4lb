﻿using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  class RegionRepository : BasicRepository<Region>, IRegionRepository
  {
    public RegionRepository (TourOfficeDbContext dbContext)
        : base(dbContext, dbContext.Regions)
    {
    }
  }
}
