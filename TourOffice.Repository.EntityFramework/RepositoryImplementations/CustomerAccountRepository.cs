﻿using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  public class CustomerAccountRepository : BasicRepository<CustomerAccount>, ICustomerAccountRepository
  {
    public CustomerAccountRepository (TourOfficeDbContext dbContext)
        : base(dbContext, dbContext.CastomerAccounts)
    {
    }
  }
}
