﻿using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  public class ServiceRepository : BasicRepository<Service>, IServiceRepository
  {
    public ServiceRepository(TourOfficeDbContext dbContext) 
      : base(dbContext, dbContext.Services)
    {
    }
  }
}
