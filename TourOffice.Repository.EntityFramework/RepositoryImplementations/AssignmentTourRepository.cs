﻿using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  public class AssignmentTourRepository : BasicRepository<AssignmentTour>, IAssignmentTourRepository
  {
    public AssignmentTourRepository (TourOfficeDbContext dbContext)
        : base(dbContext, dbContext.AssignmentTours)
    {
    }
  }
}
