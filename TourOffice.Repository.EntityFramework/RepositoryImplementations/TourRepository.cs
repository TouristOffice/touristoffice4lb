﻿using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  class TourRepository : BasicRepository<Tour>, ITourRepository
  {
    public TourRepository (TourOfficeDbContext dbContext)
        : base(dbContext, dbContext.Tours)
    {
    }
  }
}
