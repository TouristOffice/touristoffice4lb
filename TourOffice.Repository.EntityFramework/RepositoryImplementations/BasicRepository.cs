﻿using System;
using System.Data.Entity;
using System.Linq;
using TourOffice.Utils;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  public abstract class BasicRepository<T> where T : Entity
  {
    private readonly TourOfficeDbContext _dbContext;
    private readonly DbSet<T> _dbSet;

    protected BasicRepository (TourOfficeDbContext dbContext, DbSet<T> dbSet)
    {
      _dbContext = dbContext;
      _dbSet = dbSet;
    }

    protected TourOfficeDbContext GetDbContext ()
    {
      return _dbContext;
    }

    protected DbSet<T> GetDbSet ()
    {
      return _dbSet;
    }

    public void Add (T obj)
    {
      _dbSet.Add(obj);
    }

    public void Delete (T obj)
    {
      _dbSet.Remove(obj);
    }

    public IQueryable<T> LoadAll ()
    {
      return _dbSet;
    }

    public T Load (int id)
    {
      return _dbSet.Find(id);
    }

    public int Count ()
    {
      return _dbSet.Count();
    }

    public T FindByDomainId (Guid domainId)
    {
      return _dbSet.SingleOrDefault(e => e.DomainId == domainId);
    }

    public IQueryable<Guid> SelectAllDomainIds ()
    {
      return _dbSet.Select(e => e.DomainId);
    }

    public void StartTransaction ()
    {
      _dbContext.Database.BeginTransaction();
    }

    public void Commit ()
    {
      _dbContext.ChangeTracker.DetectChanges();
      _dbContext.SaveChanges();
      _dbContext.Database.CurrentTransaction.Commit();
    }

    public void Rollback ()
    {
      _dbContext.Database.CurrentTransaction.Rollback();
    }
  }
}
