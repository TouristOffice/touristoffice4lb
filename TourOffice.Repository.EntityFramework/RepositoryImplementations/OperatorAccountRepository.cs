﻿using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  class OperatorAccountRepository : BasicRepository<OperatorAccount>, IOperatorAccountRepository
  {
    public OperatorAccountRepository (TourOfficeDbContext dbContext)
        : base(dbContext, dbContext.OperatorAccounts)
    {
    }
  }
}
