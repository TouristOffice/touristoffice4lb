﻿using System;
using System.Linq;
using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  public class AccountRepository : BasicRepository<Account>, IAccountRepository
  {
    public AccountRepository (TourOfficeDbContext dbContext)
        : base(dbContext, dbContext.Accounts)
    {
    }

    public Account FindByOrder (Order o)
    {
      var operators = GetDbSet().OfType<OperatorAccount>();
      return (from a in operators where a.Orders.Any(aOrder => aOrder.DatabaseId == o.DatabaseId) select a).SingleOrDefault();
    }

    public Account FindByEmail (string email)
    {
      return GetDbSet().SingleOrDefault(a => a.Email == email);
    }
  }
}
