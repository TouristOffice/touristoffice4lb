﻿using System;
using System.Linq;
using TourOffice.Model.Mod;
using TourOffice.Repository.Mod;

namespace TourOffice.Repository.EntityFramework.RepositoryImplementations
{
  public class OrderRepository : BasicRepository<Order>, IOrderRepository
  {
    public OrderRepository (TourOfficeDbContext dbContext)
            : base(dbContext, dbContext.Orders)
    {
    }

    public IQueryable<Guid> SelectUnconfirmedIds ()
    {
      return GetDbSet().Where(o => o.Status == OrderStatus.Placed).Select(o => o.DomainId);
    }

    public IQueryable<Guid> SelectReady4DeliveryIds ()
    {
      return GetDbSet().Where(o => o.Status == OrderStatus.Delivering).Select(o => o.DomainId);
    }
  }
}
