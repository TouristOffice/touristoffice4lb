﻿using System;

namespace TourOffice.Utils.Property
{
  public class RangeProperty<T> : AbstractProperty
      where T : IComparable<T>
  {
    private T _currentValue;
    private readonly T _minValue;
    private readonly T _maxValue;
    private readonly bool _includingMin;
    private readonly bool _includingMax;

    public T Value
    {
      get { return _currentValue; }

      set
      {
        CheckValue(value);
        _currentValue = value;
      }
    }

    public RangeProperty (string paramName, T minValue, bool includingMin, T maxValue, bool includingMax)
        : base(paramName)
    {
      _minValue = minValue;
      _includingMin = includingMin;
      _maxValue = maxValue;
      _includingMax = includingMax;
    }

    protected void CheckValue (T value)
    {
      if (_includingMin && value.CompareTo(_minValue) < 0 ||
          !_includingMin && value.CompareTo(_minValue) <= 0)
      {
        throw new ArgumentException("Minimal range violated", ParamName);
      }

      if (_includingMax && value.CompareTo(_maxValue) > 0 ||
           !_includingMax && value.CompareTo(_maxValue) >= 0)
      {
        throw new ArgumentException("Maximal range violated", ParamName);
      }
    }
  }
}
