﻿using System;

namespace TourOffice.Utils.Property
{
  public class RequiredProperty<T> : AbstractProperty
  {
    private T _value;

    public T Value
    {
      get { return _value; }
      set
      {
        if (value == null)
          throw new ArgumentNullException(ParamName);

        _value = value;
      }
    }

    public RequiredProperty (string paramName)
        : base(paramName)
    {
    }
  }
}
