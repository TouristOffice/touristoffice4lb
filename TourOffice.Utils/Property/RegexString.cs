﻿using System;
using System.Text.RegularExpressions;

namespace TourOffice.Utils.Property
{
  public class RegexString : NonEmptyString
  {
    private readonly string _pattern;

    public RegexString (string name, string pattern)
        : base(name)
    {
      _pattern = pattern;
    }

    protected override void CheckValue (string value)
    {
      base.CheckValue(value);
      if (!Regex.IsMatch(value, _pattern))
        throw new ArgumentException("Invalid format", ParamName);
    }
  }
}
