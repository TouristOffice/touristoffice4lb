﻿using System;

namespace TourOffice.Utils.Property
{
  public abstract class AbstractProperty
  {
    public string ParamName { get; private set; }

    protected AbstractProperty (string paramName)
    {
      if (paramName == null)
        throw new ArgumentNullException(nameof(paramName));

      ParamName = paramName;
    }
  }
}
